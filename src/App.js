import {Component} from "react";
import Numbers from "./Numbers/Numbers";

class App extends Component {
  render() {
    return (
        <div>
          <Numbers/>
        </div>
    );
  }
}

export default App;
