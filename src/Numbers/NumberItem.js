import React, {Component} from 'react';

class NumberItem extends Component {
    render() {
        return (
            <div className="num-item">
                {this.props.number}
            </div>
        );
    }
}

export default NumberItem;