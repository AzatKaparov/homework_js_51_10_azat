import React, {Component} from 'react';
import NumberItem from "./NumberItem";
import './Numbers.css';

class Numbers extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [1, 2, 3, 4, 5],
        };
    }

    getRandom = (min, max) => {
        return Math.floor(min + Math.random() * (max - min));
    };

    setNewMassive = () => {
        const newList = [];
        while (true) {
            const newNumber = this.getRandom(5, 37);
            if (!newList.includes(newNumber)) {
                newList.push(newNumber);
            }
            if (newList.length === 5) {
                newList.sort((a, b) => a > b ? 1 : -1);
                break;
            }
        }
        this.setState({
            list: newList,
        });
    };

    render() {
        const listItems = this.state.list.map((item, idx) =>
            <NumberItem number={item} key={idx}/>
        );
        return (
            <div className="numbers">
                <button className="num-btn" onClick={this.setNewMassive}>New numbers</button>
                <div className="num-row">
                    {listItems}
                </div>
            </div>
        );
    }
}

export default Numbers;